# A reminder for the `dd` command.

## Copying the last Ubuntu LTS to /dev/sdc with a progress bar:

    sudo dd if=ubuntu-18.04.1-desktop-amd64.iso of=/dev/sdc bs=4M status=progress && sync #bs is optional

## Erasing a disk's partition table (here on /dev/sdd):

    sudo dd if=/dev/zero of=/dev/sdd bs=512 count=1